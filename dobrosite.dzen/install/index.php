<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class dobrosite_dzen extends CModule
{
    var $MODULE_ID = "dobrosite.dzen";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;

    public function __construct()
    {
        $arModuleVersion = array();
        
        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_ID = 'dobrosite.dzen';
        $this->MODULE_NAME = GetMessage('DZEN_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage("DZEN_MODULE_DESCRIPTION");
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = GetMessage("DZEN_MODULE_PARTNER_NAME");
        $this->PARTNER_URI = 'http://dobrosite.com';
    }

    public function DoInstall()
    {
        
        $this->InstallFiles();
        ModuleManager::registerModule($this->MODULE_ID);
    }

    public function DoUninstall()
    {
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dobrosite.dzen/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
    }

    function UnInstallFiles()
    {   DeleteDirFilesEx("/bitrix/components/dobrosite/yandex.dzen");
        //DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/dobrosite.dzen/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components");
    }

}
