<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("DZEN_COMP_NAME"),
	"DESCRIPTION" => GetMessage("DZEN_COMP_DESCRIPTION"),
	"ICON" => "",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "dobrosite",
		"NAME" => "Dobrosite Tools"
	),
);

?>