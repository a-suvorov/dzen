<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arResult = array();
if (CModule::IncludeModule('iblock')){
	$rsSite = CSite::GetByID(SITE_ID);
	$arSite = $rsSite->Fetch();
	$arResult["SITE_NAME"] = htmlspecialchars($arSite["SITE_NAME"] ? $arSite["SITE_NAME"] : $arSite["NAME"]);
	$arResult["SITE_URL"] = "http://".$_SERVER["SERVER_NAME"];
	$arResult["SITE_LANG"] = $arSite["LANGUAGE_ID"];
	$dbRes = CIBlockElement::GetList(Array($arParams["SORT_FIELD"]=>$arParams["SORT_ORDER"]), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y"), false, Array("nTopCount" => $arParams["COUNT"]), Array("ID", "NAME", "DETAIL_PAGE_URL" ,"PREVIEW_PICTURE", "DETAIL_PICTURE", "PREVIEW_TEXT", "DETAIL_TEXT", "TIMESTAMP_X", "DATE_CREATE"));
	while ($arRes = $dbRes->GetNext()){		
		$arResult["ITEMS"][$arRes["ID"]]["NAME"] = $arRes["NAME"];
		$arResult["ITEMS"][$arRes["ID"]]["DETAIL_PAGE_URL"] = "http://".$_SERVER["SERVER_NAME"].$arRes["DETAIL_PAGE_URL"];
		//$arResult["ITEMS"][$arRes["ID"]]["DATE"] = date(DATE_RFC822, strtotime($arRes["TIMESTAMP_X"]));
		$arResult["ITEMS"][$arRes["ID"]]["DATE"] = date("r", MkDateTime($DB->FormatDate($arRes["DATE_CREATE"], Clang::GetDateFormat("FULL"), "DD.MM.YYYY H:I:S"), "d.m.Y H:i:s"));//date(DATE_RFC822, strtotime($arRes["TIMESTAMP_X"]));
		$arResult["ITEMS"][$arRes["ID"]]["PREVIEW_TEXT"] = 	($arRes["PREVIEW_TEXT"]) ? $arRes["PREVIEW_TEXT"] : substr($arRes["DETAIL_TEXT"], 0, 300);
		$arResult["ITEMS"][$arRes["ID"]]["DETAIL_TEXT"] = getDetailDzenFormat($arRes["DETAIL_TEXT"]);
		$arResult["ITEMS"][$arRes["ID"]]["MEDIA"] = getMedia($arRes["DETAIL_TEXT"]);
	}
	$GLOBALS['APPLICATION']->RestartBuffer(); 
	$this->IncludeComponentTemplate();	
	die;
}

function getDetailDzenFormat($str){
		preg_match_all("/<img[\s\S]*>/U", $str, $matches);
		foreach ($matches[0] as $value) {
			$str = str_replace($value, "<figure>".$value."</figure>", $str);
		}
		return $str;
}

function getMedia($str){
		$arMedia = array();
		preg_match_all("/<img[\s\S]*>/U", $str, $matches);
		foreach ($matches[0] as $key=>$value) {
			preg_match("/src=[\"'](.+)[\"']/U", $value, $match_src);
			$arMedia[$key]["url"] = (strpos($match_src[1], "http") === false) ? "http://".$_SERVER["SERVER_NAME"].$match_src[1] : $match_src[1];//$img->getAttribute("src");
			$arMedia[$key]["type"] = image_type_to_mime_type(exif_imagetype($arMedia[$key]["url"]));
		}
		return $arMedia;
}



?>


