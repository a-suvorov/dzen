<?
$MESS["DZEN_COMP_IBLOCK_ID"] = "ID Инфоблока из которого берем данные";
$MESS["DZEN_COMP_LIST_TYPE"] = "Тип Инфоблока";
$MESS["DZEN_COMP_COUNT"] = "Кол-во выводимых элементов";
$MESS["DZEN_COMP_CATEGORY"] = "Категория принадлености данных";
$MESS["DZEN_COMP_SORT_FIELD"] = "Поле сортировки";
$MESS["DZEN_COMP_SORT_ORDER"] = "Порядок сортировки";