<?
header('Content-Type: application/rss+xml; charset=utf-8');
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:georss="http://www.georss.org/georss">
<channel>
	<title><?=$arResult["SITE_NAME"];?></title>
	<link><?=$arResult["SITE_URL"];?></link>
	<description>
		<?=$arResult["SITE_NAME"];?>
	</description>
	<language><?=$arResult["SITE_LANG"];?></language>
	<?foreach ($arResult["ITEMS"] as $key => $value):?>
		<item>
			<title><?=$value["NAME"]?></title>
			<link><?=$value["DETAIL_PAGE_URL"]?></link>
			<pubDate><?=$value["DATE"]?></pubDate>
			<media:rating scheme="urn:simple">nonadult</media:rating>
			<author><?=$arResult["SITE_NAME"];?></author>
			<?foreach ($arParams["CATEGORY"] as $category):?>
			<category><?=$category?></category>
			<?endforeach;?>
			<?foreach ($value["MEDIA"] as $key => $media):?>
			<enclosure url="<?=$media['url']?>" type="<?=$media['type']?>"/>
			<?endforeach;?>
			<description><![CDATA[
				<?=$value["PREVIEW_TEXT"];?>
			]]></description>
			<content:encoded><![CDATA[
				<?=$value["DETAIL_TEXT"];?>
			]]></content:encoded>
		</item>
	<?endforeach;?>	
</channel>
</rss>

