<?
CModule::IncludeModule('iblock');
$arCategories = array(
	"������������" => "������������",
    "��������" => "��������",
    "�����" => "�����",
    "��������" => "��������",
    "���������" => "���������",
    "�����" => "�����",
    "����������" => "����������",
    "�����" => "�����",
    "����" => "����",
    "������" => "������",
    "����������" => "����������",
    "����" => "����",
    "��������" => "��������",
    "����" => "����",
    "������������" => "������������",
    "����������" => "����������",
    "��������" => "��������",
    "����" => "����",
    "���" => "���",
    "�����" => "�����",
    "���" => "���",
    "������" => "������",
    "����������" => "����������",
    "����" => "����",
    "�������" => "�������",
    "�����������" => "�����������"
	);

$arSortFields = array(
       "created" => "���� ��������",
       "timestamp_x" => "���� ���������",
       "active_from" => "���� ����������",
       "sort" => "������ ����������",
       "ID" => "ID ��������",
    );

$arSortOrder = array(
        "ASC" => "�� �����������",
        "DESC" => "�� ��������"
    );

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));
$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
    $arIBlocks[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];


$arComponentParameters = array(
	"PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DZEN_COMP_LIST_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DZEN_COMP_IBLOCK_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
		),
		"COUNT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DZEN_COMP_COUNT"),
			"TYPE" => "TEXT",
		),
		"CATEGORY" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DZEN_COMP_CATEGORY"),
            "TYPE" => "LIST",
            "MULTIPLE" => 'Y',
			"VALUES" => array_conv($arCategories),
		),
        "SORT_FIELD" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DZEN_COMP_SORT_FIELD"),
            "TYPE" => "LIST",
            "VALUES" => array_conv($arSortFields),
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_ORDER" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DZEN_COMP_SORT_ORDER"),
            "TYPE" => "LIST",
            "VALUES" => array_conv($arSortOrder),
        ),
	),
);

function array_conv($arr) {
    $new_array = array();
    foreach ($arr as $key => $value) {
        $new_array[iconv("windows-1251", "UTF-8", $key)] = iconv("windows-1251", "UTF-8", $value);
    }
    return $new_array;
}

		?>